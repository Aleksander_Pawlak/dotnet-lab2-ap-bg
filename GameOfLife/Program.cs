﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Plansza
    {
        bool[,] array;

        public void DefineSize(int a, int b)
        {
            array = new bool[a, b];
        }

        private int NumberOfN(int x, int y)
        {
            int n = 0;

            for (int i = x-1; i <= x+1; i++)
            {
                for(int j = y-1; j <= y+1; j++)
                {
                    if (i >= 0 && i < array.GetLength(0) && j >= 0 && j < array.GetLength(1))
                        if (array[i, j] && !(i == x && j==y)) n++;
                }
            }

                return n;
        }

        public void check()
        {
            bool[,] tmpArray = new bool[array.GetLength(0),array.GetLength(1)];

            for(int i = 0; i < array.GetLength(0); i++){
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    int numberN = NumberOfN(i, j);

                    if (array[i, j])
                    {
                        if (numberN < 2) tmpArray[i, j] = false; 
                        if (numberN == 2 || numberN == 3) tmpArray[i,j] = true; 
                        if (numberN > 3) tmpArray[i,j] = false; 
                    }
                    else
                    {
                        if (numberN == 3) tmpArray[i, j] = true;
                        else tmpArray[i, j] = false;
                    }
                }
            }

            array = tmpArray.Clone() as bool[,];
        }

        public void display()
        {
            for(int i = 0; i < array.GetLength(0); i++){
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j]) Console.Write("#");
                    else Console.Write("0");
                }

                Console.Write(Environment.NewLine);
            }
            Console.Write(Environment.NewLine);
        }

        public void fillRandom()
        {
            Random rand = new Random();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                    array[i, j] = rand.Next(100) < 50 ? true : false;
            }
        }
    }

    class Program
    {   


        static void Main(string[] args)
        {
            Plansza plansza = new Plansza();

            int x = 10, y = 10;

            plansza.DefineSize(x, y);
            plansza.fillRandom();
            plansza.display();

            plansza.check();
            Console.Read();
            Console.Clear();
            
            while (true)
            {
                plansza.check();
                plansza.display();

                Console.Read();
                Console.Clear();
            }
        }
    }
}
